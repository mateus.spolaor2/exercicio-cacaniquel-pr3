package com.mastertech.exercicio;


public enum Slot {

    BANANA(10, "Banana"),
    FRAMBOESA(50, "Framboesa"),
    MOEDA(100, "Moeda"),
    SETE(300, "7");

    private int pontos;
    private String tipo;

    Slot(int pontos, String tipo) {
        this.pontos = pontos;
        this.tipo = tipo;
    }

    Slot() {
    }

    public int getPontos() {
        return pontos;
    }

    public void setPontos(int pontos) {
        this.pontos = pontos;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
}
