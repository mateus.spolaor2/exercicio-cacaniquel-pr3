package com.mastertech.exercicio;

import java.util.ArrayList;

public class Impressora {

    public static void imprimeBoasVindas(){
        System.out.println("Bem vindo ao Caça Niquel do Mateus!\nQuantos slots teremos no jogo?");
    }

    public static void imprimeQtdMinNaoAtingida(){
        System.out.println("Quantidade mínima não atingida, valor setado para três slots.");
    }

    public static void imprimeQtdSlots(int qtdSlots){
        System.out.println("O jogo será iniciado com " + qtdSlots + " slots.");
    }

    public static void imprimeSlotsSorteados(ArrayList<Slot> slotsSorteados) {

        System.out.print("| ");
        for (Slot slot : slotsSorteados) {
            System.out.print(slot.getTipo() + " | ");
        }

    }

    public static void imprimeResultadoFinal(Integer resultadoSomado){

        System.out.println("\nSua pontuação final foi: " + resultadoSomado);

    }




}
