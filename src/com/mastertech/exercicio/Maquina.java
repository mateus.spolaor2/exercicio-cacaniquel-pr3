package com.mastertech.exercicio;

import java.util.ArrayList;
import java.util.Scanner;

public class Maquina {

    private Integer qtdSlots;

    private Sorteador sorteador;

    public Maquina(Integer qtdSlots, Sorteador sorteador) {
        this.qtdSlots = qtdSlots;
        this.sorteador = sorteador;
    }

    public Maquina(){}

    public Integer getQtdSlots() {
        return qtdSlots;
    }

    public void setQtdSlots(Integer qtdSlots) {
        this.qtdSlots = qtdSlots;
    }

    public Sorteador getSorteador() {
        return sorteador;
    }

    public void setSorteador(Sorteador sorteador) {
        this.sorteador = sorteador;
    }

    public ArrayList<Slot> montaSlotsDoJogo(){

        ArrayList<Slot> slotsValidos = new ArrayList<>();

        slotsValidos.add(Slot.BANANA);
        slotsValidos.add(Slot.FRAMBOESA);
        slotsValidos.add(Slot.MOEDA);
        slotsValidos.add(Slot.SETE);

        for (int i = 0; i < slotsValidos.size(); i++) {
            System.out.println(slotsValidos.get(i).getTipo() + " de valor - " + slotsValidos.get(i).getPontos());
        }

        defineQuantidadeDeSlots();

        return slotsValidos;
    }

    private void defineQuantidadeDeSlots() {
        Scanner scanner = new Scanner(System.in);
        Impressora.imprimeBoasVindas();
        int nroSlots = scanner.nextInt();
        if (nroSlots < 3) {
            Impressora.imprimeQtdMinNaoAtingida();
            this.qtdSlots = 3;
        } else {
            Impressora.imprimeQtdSlots(nroSlots);
            this.qtdSlots = nroSlots;
        }
    }

    public void ligaMaquina(){
        Sorteador sorteador = new Sorteador();

        ArrayList<Slot> slotsValidos = new ArrayList<>(montaSlotsDoJogo());
        rodaJogo(sorteador, slotsValidos);

    }

    private void rodaJogo(Sorteador sorteador, ArrayList<Slot> slotsValidos) {
        ArrayList<Slot> slotsSorteados;
        slotsSorteados = sorteador.sortearSlots(this.qtdSlots, slotsValidos);
        Impressora.imprimeSlotsSorteados(slotsSorteados);

        Boolean temBonus = Calculadora.temBonus(slotsSorteados, slotsValidos);

        Impressora.imprimeResultadoFinal(Calculadora.calculaResultado(slotsSorteados, temBonus));
    }

}
