package com.mastertech.exercicio;

import java.util.ArrayList;

public class Sorteador {

    public ArrayList<Slot> sortearSlots (Integer qtdSlots, ArrayList<Slot> slotsDisponiveis){
        ArrayList<Slot> sorteio = new ArrayList<>();
        System.out.println("Resultado:");
        for (int i = 0; i < qtdSlots; i++) {
            int slotSorteado = (int)(Math.random() * (slotsDisponiveis.size()));
            sorteio.add(slotsDisponiveis.get(slotSorteado));
        }
        return sorteio;
    }

}
