package com.mastertech.exercicio;

import java.util.ArrayList;

public class Calculadora {

    public static Boolean temBonus (ArrayList<Slot> slotsSorteados, ArrayList<Slot> slotsValidos) {

        for (Slot slotValido : slotsValidos){
            int repeticoes = 0;
            for (Slot slotSorteado : slotsSorteados){
                if(slotSorteado.getTipo().equals(slotValido.getTipo())){
                    repeticoes++;
                    if(repeticoes == 3) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static Integer calculaResultado(ArrayList<Slot> slotsSorteados, Boolean temBonus){

        int resultadoSomado = 0;

        for (Slot slot : slotsSorteados) {
            resultadoSomado += slot.getPontos();
        }

        if(temBonus) {
            return resultadoSomado*100;
        } else {
            return resultadoSomado;
        }
    }
}
